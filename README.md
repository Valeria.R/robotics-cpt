/* Final Project For Robotics
 *By Valeria Ramirez
 *  Function: A program that makes car move with motors and follow a black line on a track. 
 *  Moves by using sensors to read between black and white 
 *Program is read on an arduino board on a robotic car. Using the set rules the car moves; wheels turning and motors turning on and off.
 * Version 1
 * Jan 7, 2019
 */